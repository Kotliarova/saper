import './index.html';
import './index.scss';


let arrAllSquare = [];
const svgBomb = '<svg class="square-bomb" width="25" xmlns="http://www.w3.org/2fff/svg" viewBox="0 0 512 512"><path d="M459.1 52.4L442.6 6.5C440.7 2.6 436.5 0 432.1 0s-8.5 2.6-10.4 6.5L405.2 52.4l-46 16.8c-4.3 1.6-7.3 5.9-7.2 10.4c0 4.5 3 8.7 7.2 10.2l45.7 16.8 16.8 45.8c1.5 4.4 5.8 7.5 10.4 7.5s8.9-3.1 10.4-7.5l16.5-45.8 45.7-16.8c4.2-1.5 7.2-5.7 7.2-10.2c0-4.6-3-8.9-7.2-10.4L459.1 52.4zm-132.4 53c-12.5-12.5-32.8-12.5-45.3 0l-2.9 2.9C256.5 100.3 232.7 96 208 96C93.1 96 0 189.1 0 304S93.1 512 208 512s208-93.1 208-208c0-24.7-4.3-48.5-12.2-70.5l2.9-2.9c12.5-12.5 12.5-32.8 0-45.3l-80-80zM200 192c-57.4 0-104 46.6-104 104v8c0 8.8-7.2 16-16 16s-16-7.2-16-16v-8c0-75.1 60.9-136 136-136h8c8.8 0 16 7.2 16 16s-7.2 16-16 16h-8z"/></svg>';
const svgFlag = '<svg class="square-flag" width="25" xmlns="http://www.w3.org/2fff/svg" viewBox="0 0 448 512"><path d="M64 32C64 14.3 49.7 0 32 0S0 14.3 0 32V64 368 480c0 17.7 14.3 32 32 32s32-14.3 32-32V352l64.3-16.1c41.1-10.3 84.6-5.5 122.5 13.4c44.2 22.1 95.5 24.8 141.7 7.4l34.7-13c12.5-4.7 20.8-16.6 20.8-30V66.1c0-23-24.2-38-44.8-27.7l-9.6 4.8c-46.3 23.2-100.8 23.2-147.1 0c-35.1-17.6-75.4-22-113.5-12.5L64 48V32z"/></svg>';
const desk = document.querySelector('.desk');
const btnRestart = document.querySelector('.btn-restart');
const btnStartNewGame = document.querySelector('.btn-new-game');
const squareHtml = document.querySelectorAll('.square');
const gameOverScreen = document.querySelector('.game-over');
let currentArr = [];
let bombNearOpenSquare = 0;
let currentSquareForAddNumber;
let emptySquares = [];
let notOpenedSquares = [];
let square;
let currentSquare;
let quantityClicks = 0;
let arrNumberCheckedSquares = [];
const arrForSquaresAll = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 16, 24, 32, 40, 48, 56], [0, 1, 2, 3, 4, 5, 6, 7], [0, 1, 2, 3, 4, 5, 6, 7, 15, 23, 31, 39, 47, 55, 63], [0, 8, 16, 24, 32, 40, 48, 56], [7, 15, 23, 31, 39, 47, 55, 63], [0, 8, 16, 24, 32, 40, 48, 56, 57, 58, 59, 60, 61, 62, 63],[56, 57, 58, 59, 60, 61, 62, 63], [7, 15, 23, 31, 39, 47, 55, 56, 57, 58, 59, 60, 61, 62, 63]];
const fontSize = '25px';
const htmlFlags = document.querySelector('.flags');
const htmlStandFlag = document.querySelector('.stand-flag');
let defoultStandFlags = 10;

window.addEventListener('load', even => {
    createArrSquare(64)
    console.log(arrAllSquare)
    createDesk();
    btnRestart.style.display = 'none';
    quantityClick();
    start();
    continueGame();
    standFlag();
    btnRestart.addEventListener('click', restart)
})
function createArrSquare(quantity) {
    for(let i = 0; i < quantity; i++) {
        let squareObj = {
            name: 'square',
            index: 0,
            open: false,
            flag: false
        }
        arrAllSquare.push(squareObj)
    }
}
function start() {
    desk.addEventListener('click', ev => {
        ev.preventDefault();
        currentSquare = ev.target;
        btnRestart.style.display = 'flex';
        htmlFlags.style.display = 'flex';
        if (ev.button === 0 ) {
            for (let i = 0; i < arrAllSquare.length; i++) {
                if(currentSquare.classList.contains(i) && arrAllSquare[i].name === 'square') {
                    if (quantityClicks === 1) {
                        generaiteBomb(10, 64-1, i);
                        checkAruondSquares(i, false);
                        addNumberOnSquare(bombNearOpenSquare, i);
                    }
                }
            }
        }
    }, {once: true})
}
function quantityClick() {
    desk.addEventListener('click', ev => quantityClicks++)
}
function continueGame() {
    desk.addEventListener('click', ev => {
        if (quantityClicks > 1) {
            currentSquare = ev.target;
            if (ev.button === 0 ) {
                for (let i = 0; i < arrAllSquare.length; i++) {
                    if(currentSquare.classList.contains(i) && arrAllSquare[i].name === 'bomb') {
                        isBomb(i)
                    } 
                    if(currentSquare.classList.contains(i) && arrAllSquare[i].name === 'square') {
                        checkAruondSquares(i, false);
                        addNumberOnSquare(bombNearOpenSquare, i);
                    }
                }
            }
        }
    })
}
function isBomb(numberCurrentSquare){
    if (arrAllSquare[numberCurrentSquare].name === 'bomb') {
        gameOverScreen.style.display = 'block'
        for (let j = 0; j < arrAllSquare.length; j++) {
            if (arrAllSquare[j].name === 'bomb') { 
                let allSquareBomb = document.querySelector(`.square-${j}`);
                allSquareBomb.innerHTML = svgBomb;
                allSquareBomb.style.backgroundColor = '#fff';
            }
        }
    }
    btnStartNewGame.addEventListener('click', restart)
}
function generaiteBomb(quantity, max, numberFirstSquare) {
    while ((arrAllSquare[numberFirstSquare].name === 'square')) {
        for(let i = 0; i < quantity; i++) {
            let numberSquareWithBomb = Math.round((Math.random() * max))
            arrAllSquare[numberSquareWithBomb].name = 'bomb';
        }
        break
    }
    return arrAllSquare
}
function cleanAll() {
    currentArr = [];
    bombNearOpenSquare = 0;
    emptySquares = [];
    notOpenedSquares = [];
    btnRestart.style.display = 'none';
    gameOverScreen.style.display = 'none'
    htmlFlags.style.display = 'none';
    arrAllSquare.forEach(elem => {
        elem.name = 'square';
        elem.index = 0;
        elem.open = false;
        elem.flag = false;
    })
    return arrAllSquare;
}
function restart() {
    let oldSquare = document.querySelectorAll('.square');
            oldSquare.forEach( elem => {
                elem.remove();
        })
        cleanAll();
        createDesk();
        defoultStandFlags = 10;
        htmlStandFlag.innerText = `${defoultStandFlags}`;
        quantityClicks = 0;
        start();
        continueGame();
        standFlag();
}
function createDesk() {
    for(let i = 0; i < arrAllSquare.length; i++) {
        arrAllSquare[i].index = i;
        square = document.createElement('div');
        square.classList.add('square');
        square.classList.add(`square-${i}`);
        square.classList.add(`${i}`);
        desk.append(square);
    }
}
function checkSquare(numberNeighbour) {
    if(numberNeighbour >= 0 && numberNeighbour < arrAllSquare.length) {
        if (arrAllSquare[numberNeighbour].name === 'bomb') {
            bombNearOpenSquare++;
        }
        currentArr.push(numberNeighbour);
    };
}
function countNumberCheckedSquares(numberCurrentSquare) {
    let numberCheckedSquareOne = -9 + numberCurrentSquare;
    let numberCheckedSquareTwo = -8 + numberCurrentSquare;
    let numberCheckedSquareThree = -7 + numberCurrentSquare;
    let numberCheckedSquareFour = numberCurrentSquare - 1;
    let numberCheckedSquareFive = numberCurrentSquare + 1;
    let numberCheckedSquareSix = 7 + numberCurrentSquare;
    let numberCheckedSquareSeven = 8 + numberCurrentSquare;
    let numberCheckedSquareEight = 9 + numberCurrentSquare;
    return arrNumberCheckedSquares = [numberCheckedSquareOne, numberCheckedSquareTwo, numberCheckedSquareThree, numberCheckedSquareFour, numberCheckedSquareFive, numberCheckedSquareSix, numberCheckedSquareSeven, numberCheckedSquareEight]
}
function checkAruondSquares(numberCurrentSquare, withOrNot) {
    currentArr = [];
    bombNearOpenSquare = 0;
    countNumberCheckedSquares(numberCurrentSquare);
    for(let i = 0; i < 8; i++) {
        if (!(arrForSquaresAll[i].includes(numberCurrentSquare))) {
            checkSquare(arrNumberCheckedSquares[i])
        }
    }
    currentSquareForAddNumber = document.querySelector(`.square-${numberCurrentSquare}`);
    if (withOrNot) {
        return currentArr, bombNearOpenSquare, currentSquareForAddNumber
    } else {
        return bombNearOpenSquare, currentSquareForAddNumber
    }
    
}
function putNumber(currentSquareForAddNumber, number, num, color) {
    currentSquareForAddNumber.innerText = `${num}`;
    currentSquareForAddNumber.style.fontSize = fontSize;
    currentSquareForAddNumber.style.backgroundColor = '#fff';
    arrAllSquare[number].open = true;
    currentSquareForAddNumber.style.color = `${color}`;
}
function addNumberOnSquare(bombs, number) {
    switch(bombs) {
        case 0:
            currentSquareForAddNumber.style.backgroundColor = '#fff';
            arrAllSquare[number].open = true;
            openEmptySquare(number);
            while(emptySquares.length) {
                runOnEmptySquares(emptySquares)
            }
            break;
        case 1: 
            putNumber(currentSquareForAddNumber, number, '1', 'blue')
            break;
        case 2: 
            putNumber(currentSquareForAddNumber, number, '2', 'green')
            break;
        case 3: 
            putNumber(currentSquareForAddNumber, number, '3', 'red')
            break;
        case 4: 
            putNumber(currentSquareForAddNumber, number, '4', '#1d2f5f')
            break;
        case 5: 
            putNumber(currentSquareForAddNumber, number, '5', 'brown')
            break;
        case 6: 
            putNumber(currentSquareForAddNumber, number, '6', '#2ddfe6')
            break;
        case 7: 
            putNumber(currentSquareForAddNumber, number, '7', 'black')
            break;
        case 8: 
            putNumber(currentSquareForAddNumber, number, '8', 'white')
            break;
    }
}
function openEmptySquare(number){
    notOpenedSquares = [];
    emptySquares = [];
    checkAruondSquares(number, true);
    currentArr.forEach(square => {
        checkAruondSquares(square, false);
        if(!bombNearOpenSquare) {
            currentSquareForAddNumber.style.backgroundColor = '#fff';
            arrAllSquare[square].open = true;
            emptySquares.push(square);
        } else {
            addNumberOnSquare(bombNearOpenSquare, square);
        }
    });
    return emptySquares;

}
function runOnEmptySquares(emptySquaresP) {
    emptySquares = [];
    emptySquaresP.forEach(number => {
        checkAruondSquares(number, true);
        checkOpenAroundSquares(currentArr);
        notOpenedSquares.forEach(square => {
            checkAruondSquares(square, false);
            if(!bombNearOpenSquare) {
                currentSquareForAddNumber.style.backgroundColor = '#fff';
                arrAllSquare[square].open = true;
                emptySquares.push(square);
            } else {
                addNumberOnSquare(bombNearOpenSquare, square);
            }
        });
    });
    return emptySquares;
}
function checkOpenAroundSquares(currentArr) {
    notOpenedSquares = [];
    currentArr.forEach(number => {
        if (arrAllSquare[number].index === number && arrAllSquare[number].open === false && !emptySquares.includes(arrAllSquare[number].index)) {
            notOpenedSquares.push(number);
        }
    })
    return notOpenedSquares;
}
function standFlag() {
   desk.addEventListener('contextmenu', ev => {
        ev.preventDefault();
        currentSquare = ev.target;
        for (let i = 0; i < arrAllSquare.length; i++){
            if(currentSquare.classList.contains(i) && arrAllSquare[i].flag === false && arrAllSquare[i].open === false ) {
                arrAllSquare[i].flag = true;
                btnRestart.style.display = 'flex';
                htmlFlags.style.display = 'flex';
                currentSquare.innerHTML = svgFlag;
                currentSquare.style.backgroundColor = '#fff';
                defoultStandFlags--;
                htmlStandFlag.innerText = `${defoultStandFlags}`
            }  
        }
    })
}
// При подвійному кліку на клітинку з цифрою - якщо навколо неї встановлено таку ж кількість прапорців, що зазначено на цифрі цієї комірки, відкривати всі сусідні комірки.


